var mongoose = require('mongoose');

var db = mongoose.connection;
//mongoose.connect('mongodb://localhost/test');
mongoose.connect('mongodb://go84:go84@192.168.101.125/test');

var locationData = [{
    x: 1.14515,
    y: 2.161,
    timestamp: 10111011,
    mac: '12asfatq11'
},{
    x: 2,
    y: 1,
    timestamp: 1011151011,
    mac: '12fggggg'
}
];



db.on('error', console.error);
db.once('open', function() {
    // Create your schemas and models here.
    var locationSchema = new mongoose.Schema({
        x: Number,
        y: Number,
        timestamp: Number,
        mac: String
    });

    var Location = mongoose.model('Location', locationSchema);

    Location.create(locationData, function(err, output) {
        if (err) return console.error(err);
        //res.send(output);
        console.log(output);
        mongoose.connection.close();
    });
});